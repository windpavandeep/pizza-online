import { ACTION_TYPES } from "./actionTypes";
import firebase from "firebase"


export const loginSuccess = (data) => {
  return (dispatch) => {
    dispatch({
      type: ACTION_TYPES.LOGIN_USER_SUCCESS,
      payload: data
    });
  }
}

export const loginError = (data) => {
  return (dispatch) => {
    dispatch({
      type: ACTION_TYPES.LOGIN_USER_ERROR,
      payload: data
    })
  }
}

export const messageUpdate = (msg) => {
  return (dispatch) => {
    dispatch({
      type: ACTION_TYPES.GLOBAL_MSG_UPDATE,
      payload: msg
    })
  }
}

export const getOrderHistory = (email) => {
  return async (dispatch) => {
    let list = [];
    const document = await firebase.firestore().collection("mst-order").where('email','==', email).get();
    document.forEach((doc) => {
      const response = {
        id: doc.id,
        delivered_name: doc.data().surname,
        date: doc.data().date,
        city: doc.data().city,
        mobile: doc.data().mobile,
        amount: `$${doc.data().totalAmount}`,
        products: doc.data().productIds
      };
      list.push(response)
    });
    dispatch({
      type: ACTION_TYPES.GET_ORDER_HISTORY,
      payload: list
    })
  }
}