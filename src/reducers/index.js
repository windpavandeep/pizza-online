import { combineReducers } from 'redux';
import authoringContentStore from './authoringContentStore';

const reducers = combineReducers({
    authoringContentStore
});

export default reducers;