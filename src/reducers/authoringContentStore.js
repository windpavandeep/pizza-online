import { ACTION_TYPES } from '../actions/actionTypes';
import { updateObject } from '../utils';

export const initialState = {
    user: {},
    isLoading: true,
    globalMsg: '',
    isAuth: false,
    orderHistory: []
}

const authoringContentStore = (state = initialState, { type, payload }) => {
    switch (type) {
        case ACTION_TYPES.LOGIN_USER_SUCCESS:
            return updateObject(state, { user: payload, isLoading: false, isAuth: true })

        case ACTION_TYPES.LOGIN_USER_ERROR:
            return updateObject(state, { globalMsg: 'Something went wrong please try again', isLoading: false, isAuth: false })

        case ACTION_TYPES.GLOBAL_MSG_UPDATE:
            return updateObject(state, { globalMsg: payload })

        case ACTION_TYPES.GET_ORDER_HISTORY:
            return updateObject(state, { orderHistory: payload })

        default:
            return { ...state }
    }
}

export default authoringContentStore