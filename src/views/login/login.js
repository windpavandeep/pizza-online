import React, { Component } from 'react';
import './login.scss'
import * as userProfileDispatcher from "../../actions/userProfileDispatcher"
import * as firebase from "firebase/app";
import { auth } from "../../config/index";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';


const provider = new firebase.auth.GoogleAuthProvider();

class Login extends Component {

    componentDidMount() {
        provider.setCustomParameters({
            promt: "select_account",
        });
    }

    login = async () => {
        let user;
        await auth.signInWithPopup(provider)
        auth.onAuthStateChanged(userAuth => {
            let user = {
                displayName: userAuth.displayName,
                email: userAuth.email,
                photoURL: userAuth.photoURL,

            }
            setTimeout(() => {
                this.props.actions.loginSuccess(user)
                localStorage.setItem('user', JSON.stringify(user))
                localStorage.setItem('isAuth', true)
            }, 2000)
        });
    }

    render() {
        return (
            <div className="main">
                <p className="sign" align="center">Sign in</p>
                <div className="form1">
                    <button className="submit" onClick={this.login}>Sign in with Google</button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    user: state.authoringContentStore,
    globalMsg: state.authoringContentStore.globalMsg,
    isLoading: state.authoringContentStore.isLoading
})

const mapDispatchToProps = (dispath) => ({
    actions: bindActionCreators({ ...userProfileDispatcher }, dispath)
})

export default connect(mapStateToProps, mapDispatchToProps)(Login);