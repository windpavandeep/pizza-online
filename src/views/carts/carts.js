import React, { Component } from "react"
import firebase from "firebase"
import { toast } from 'react-toastify';
import ProductCard from "../../components/product-card/product-card"
import * as userProfileDispatcher from "./../../actions/userProfileDispatcher"
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import history from "./../../routes/history"
import "./carts.scss"


class Carts extends Component {
    state = {
        pizzaList: [],
        productIds: [],
        totalAmount: 0,
        orderNowCollapse: false,
        address: '',
        surname: '',
        city: '',
        mobile: '',
        usd_euro: localStorage.getItem('usd_euro')
    }

    async componentDidMount() {
        const { user } = this.props.user
        let list = []
        let productIds = [];
        await firebase.firestore().collection("mst-cart").where('email', '==', user.email).get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                this.setState({
                    totalAmount: this.state.totalAmount + (parseInt(doc.data().item.rate) * doc.data().qty)
                })
                console.log(" == ", doc.data().qty)
                productIds.push({
                    productId: doc.data().productId,
                    title: doc.data().item.title,
                    rate: doc.data().item.rate,
                    qty: doc.data().qty
                })
                list.push({
                    id: doc.id,
                    data: doc.data()
                })
            });
        });

        const currentdate = new Date()
        var datetime = currentdate.getDate() + "/" + (currentdate.getMonth() + 1) + "/" + currentdate.getFullYear()
        this.setState({
            email: user.email,
            pizzaList: list,
            productIds: productIds,
            date: datetime
        })

    }

    onRemoveCart = async () => {
        const { user } = this.props.user
        let list = []
        let productIds = [];
        this.setState({
            totalAmount: 0
        })
        await firebase.firestore().collection("mst-cart").where('email', '==', user.email).get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                this.setState({
                    totalAmount: this.state.totalAmount + (parseInt(doc.data().item.rate) * doc.data().qty)
                })
                productIds.push({
                    productId: doc.data().productId,
                    rate: doc.data().item.rate,
                    qty: doc.data().qty
                })
                list.push({
                    id: doc.id,
                    data: doc.data()
                })
            });
        });
        this.setState({
            pizzaList: list,
            productIds: productIds,
        })
    }

    payNow() {
        this.setState({
            orderNowCollapse: !this.state.orderNowCollapse
        })
    }

    onChangeTextArea = (input, event) => {
        switch (input) {
            case 'address':
                this.setState({
                    address: event.target.value
                })
                break;
            case 'surname':
                this.setState({
                    surname: event.target.value,
                })
                break;
            case 'city':
                this.setState({
                    city: event.target.value,
                })
                break;
            case 'mobile':
                this.setState({
                    mobile: event.target.value,
                })
                break;

            default:
                break;
        }
    }

    onOrderSubmit = async () => {
        const payload = this.state;
        const cartList = payload.pizzaList;
        delete payload.orderNowCollapse
        delete payload.pizzaList;

        if (payload.totalAmount > 0) {
            await firebase.firestore().collection("mst-order").add(payload).then(() => {
                cartList.map((item, key) => {
                    firebase.firestore().collection('mst-cart').doc(item.id).delete()
                })
                this.props.actions.messageUpdate("Order Success")
                history.push('/')
            })
        } else {
            toast.error("Please Cart some products from Menus");
        }

    }

    onChangeQty = async (docId, value, key) => {
        const { user } = this.props.user
        let cart = this.state.pizzaList.filter((res) => res.id === docId)
        cart[0].data.qty = value
        const newData = this.state.pizzaList
        newData[key] = cart[0];
        let productIds = [];
        this.setState({
            totalAmount: 0
        })
        await firebase.firestore().collection('mst-cart').doc(docId).update({
            qty: parseInt(value)
        })
        await firebase.firestore().collection("mst-cart").where('email', '==', user.email).get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                console.log(" ==== ", doc.data())
                productIds.push({
                    productId: doc.data().productId,
                    rate: doc.data().item.rate,
                    qty: doc.data().qty,
                    title: doc.data().item.title
                })
                this.setState({
                    totalAmount: this.state.totalAmount + (parseInt(doc.data().item.rate) * doc.data().qty),
                    productIds: productIds
                })
            });
        });
        this.setState({
            pizzaList: newData
        })
    }

    render() {
        return (
            <div className="carts">
                {
                    this.state.pizzaList && this.state.pizzaList.map((item, key) => {
                        return (
                            <ProductCard index={key} onChangeQty={this.onChangeQty} qty={item.data.qty} data={item.data.item} id={item.id} key={key} cart={true} removeCall={this.onRemoveCart} />
                        )
                    })
                }
                <div className={`cart_total ${this.state.orderNowCollapse ? 'open' : ''}`}>
                    <div className="pay_btn">
                        <div onClick={() => this.payNow()} className="button">
                            <a href="#"><span>Total Amount : ${this.state.totalAmount} / €{(this.state.usd_euro * this.state.totalAmount).toFixed(1)}  </span></a><a className="cart-btn" href="#"><i className="cart-icon ion-bag"></i>{this.state.orderNowCollapse ? 'Pay later' : 'Pay Now'}</a>
                        </div>
                    </div>
                    <div style={{ display: this.state.orderNowCollapse ? 'flex' : 'none' }} className="form">
                        <div className="form-input">
                            <label className="label">Address</label>
                            <input onChange={(e) => this.onChangeTextArea('address', e)} type="text" name="address" className="input" value={this.state.address} />
                        </div>
                        <div className="form-input">
                            <label className="label">Surname</label>
                            <input onChange={(e) => this.onChangeTextArea('surname', e)} type="text" name="surname" className="input" value={this.state.surname} />
                        </div>
                        <div className="form-input">
                            <label className="label">City</label>
                            <input type="text" onChange={(e) => this.onChangeTextArea('city', e)} name="city" className="input" value={this.state.city} />
                        </div>
                        <div className="form-input">
                            <label className="label">Mobile No</label>
                            <input type="text" onChange={(e) => this.onChangeTextArea('mobile', e)} name="mobile" className="input" value={this.state.mobile} />
                        </div>
                        <div className="form-input">
                            <input type="button" onClick={() => this.onOrderSubmit()} name="submit" className="input submit" value="Submit" />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    user: state.authoringContentStore,
    globalMsg: state.authoringContentStore.globalMsg,
    isLoading: state.authoringContentStore.isLoading
})

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators({ ...userProfileDispatcher }, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(Carts)