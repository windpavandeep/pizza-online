import React, { Component } from "react"
import ProductCard from "../../components/product-card/product-card"
import firebase from "firebase"
import * as userProfileDispatcher from "./../../actions/userProfileDispatcher"
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import "./menu.scss"

class Menu extends Component {
    state = {
        pizzaList:""
    }

    async componentDidMount() {
        let list = []
        await firebase.firestore().collection("mst-pizza").get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                const response = {
                    id: doc.id,
                    data: doc.data()
                } 
                list.push(response)
            });
        });
        this.setState({
            pizzaList: list
        })
     
    }

    render() {
        const { user } = this.props.user;
        return (
            <div className="menus">
                {
                    this.state.pizzaList && this.state.pizzaList.map((item, key) => {
                        return(
                            <ProductCard { ...item} key = { key } qty = {1} user = { user }/>
                        )
                    })
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    user: state.authoringContentStore,
    globalMsg: state.authoringContentStore.globalMsg,
    isLoading: state.authoringContentStore.isLoading
})

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators({ ...userProfileDispatcher }, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(Menu)