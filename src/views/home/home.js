import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import firebase from "firebase"
import * as userProfileDispatcher from "./../../actions/userProfileDispatcher"
import './home.scss';
import history from '../../routes/history';
import DataTable from '../../components/data-table';
import Dialog from '@material-ui/core/Dialog';

class Home extends Component {


    state = {
        headers: [
            {
                title: 'Date',
                field: "date"
            },
            {
                title: 'Order Deliverd',
                field: "delivered_name"
            },
            {
                title: 'City',
                field: "city"
            },
            {
                title: 'Mobile',
                field: "mobile"
            }, {
                title: 'Amount',
                field: "amount"
            }, {
                title: 'Action',
                field: "action",
                sorting: false,
                render: (data) => {
                    return <span onClick={() => {
                        console.log(' === data === ', data.products)
                        this.setState({
                            productRows: data.products,
                            openModal: !this.state.openModal
                        })
                    }} className="edit_button">Products Detail</span>
                }
            }
        ],
        tableRows: [],
        productHeader: [
            {
                title: 'Product Name',
                field: "title",
            },
            {
                title: 'Qty',
                field: "qty"
            },
            {
                title: 'Rate',
                field: 'rate'
            }
        ],
        productRows: [],
        openModal: false
    }

    async componentDidMount() {
        const { user: { user }, actions } = this.props;
        await actions.getOrderHistory(user.email);
    }

    onCloseModal = () => {
        this.setState({
            openModal: !this.state.openModal
        })
    }
    render() {
        const { user: { user }, orderHistory } = this.props;
        return (
            <div className="frame">
                <Dialog
                    open={this.state.openModal}
                    onClose={this.onCloseModal}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                    classes={{
                        paper:'frame-modal-paper'
                    }}
                >
                    <div>
                        <DataTable headers={this.state.productHeader} tableRows={this.state.productRows} />
                    </div>
                </Dialog>
                <div className="profile">
                    <div className="image">
                        <div className="circle-1"></div>
                        <div className="circle-2"></div>
                        <img src={user.photoURL} width="70" height="70" alt="Jessica Potter" />
                    </div>

                    <div className="name">{user.displayName}</div>
                    <div className="job">{user.email}</div>

                    <div className="actions">
                        <button onClick={() => history.push('/profile')} className="btn">Edit Profile</button>
                    </div>
                </div>

                <div className="stats">
                    <DataTable headers={this.state.headers} tableRows={orderHistory} />
                </div>
            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    user: state.authoringContentStore,
    globalMsg: state.authoringContentStore.globalMsg,
    isLoading: state.authoringContentStore.isLoading,
    orderHistory: state.authoringContentStore.orderHistory
})

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators({ ...userProfileDispatcher }, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(Home);;