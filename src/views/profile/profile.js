import React, { Component } from "react"
import firebase from "firebase"
import * as userProfileDispatcher from "./../../actions/userProfileDispatcher"
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import "./profile.scss"


class Profile extends Component {

    render() {
        const { user: { user } } = this.props
        return (
            <div className="frame">
                <div className="profile">
                    <div className="image">
                        <div className="circle-1"></div>
                        <div className="circle-2"></div>
                        <img src={ user.photoURL } width="70" height="70" alt="Jessica Potter" />
                    </div>

                    <div className="name">{ user.displayName }</div>
                    <div className="job">{ user.email }</div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    user: state.authoringContentStore,
    globalMsg: state.authoringContentStore.globalMsg,
    isLoading: state.authoringContentStore.isLoading
})

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators({ ...userProfileDispatcher }, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(Profile)