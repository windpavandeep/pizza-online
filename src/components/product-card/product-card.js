import React from "react"
import "./product-card.scss"
import firebase from "firebase"


const ProductCard = (props) => {

    const addToCart = async (docId, item) => {
        await firebase.firestore().collection("mst-cart").add({
            productId: docId,
            email: props.user.email,
            qty: 1,
            item
        })
    }

    const removeCart = async (docId) => {
        await firebase.firestore().collection("mst-cart").doc(docId).delete().then(() => {
            props.removeCall()
        }).catch((err) => {
            console.log(" === Fail === ", err)
        })
    }

    const onChangeQty = (event) => {
        props.onChangeQty(props.id, event.target.value, props.index)
    }

    return (
        <div className="menu_wrapper">
            <div className="outer">
                <div className="content animated fadeInLeft">
                    <span className="bg animated fadeInDown">EXCLUSIVE</span>
                    <h1>{props.data.title}</h1>
                    <p>{props.data.desc}</p>

                    {
                        !props.cart ?
                            <div onClick={() => addToCart(props.id, props.data)} className="button">
                                <a href="#">${props.data.rate * props.qty }</a><a className="cart-btn" href="#"><i className="cart-icon ion-bag"></i>ADD TO CART</a>
                            </div>
                            :
                            <div onClick={() => removeCart(props.id, props.data)} className="button">
                                <a href="#">${ props.data.rate * props.qty }</a><a className="cart-btn" href="#"><i className="cart-icon ion-bag"></i>Remove from CART</a>
                            </div>
                    }
                    {
                        props.cart &&
                        <div className="qty">
                            <input type="number" onChange={ onChangeQty } value={props.qty} />
                        </div>
                    }

                </div>
                <img src={props.data.image} className="animated fadeInRight" />
            </div>
        </div>
    )
}

export default ProductCard