import React from 'react';
import Logo from '../logo';
import './header.scss'
import UserMenu from '../user-menu';

const Header = (props) => {
  return(
    <div className="header">
      <Logo />
      {
        props.isAuth && <UserMenu { ...props } />
      }
    </div>
  );
}


export default Header;
