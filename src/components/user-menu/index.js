import React, { useState } from "react";
import "./user-menu.scss";
import { nav } from "../../utils/navigation";
import Badge from '@material-ui/core/Badge';
import { Link } from "react-router-dom";
import firebase from "firebase"
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import history from "../../routes/history";


const UserMenu = (props) => {

  const [count, setCounter] = useState(0);
  const { user } = props.user
  firebase.firestore().collection("mst-cart").where('email', '==', user.email).get().then((querySnapshot) => {
    setCounter(querySnapshot.docs.length)
  });

  const onLogout = () =>{
    localStorage.removeItem('user')
    localStorage.removeItem('isAuth')
    window.location.reload()
  }

  return (
    <div className={`usermenu`}>
      {
        nav.map((item, index) => {
          return (
            <Link className="usermenu_item" key={index} to={item.route}>
              <Badge badgeContent={count} invisible={!item.bages} color="primary">
                {item.icon} {item.title}
              </Badge>
            </Link>
          )
        })
      }
      <Link to = "/" onClick={ () => onLogout() }  className="usermenu_item">
        <ExitToAppIcon />
      </Link>
    </div>
  );
};

export default UserMenu;
