import React from 'react';
import {
  Link
} from "react-router-dom";
import logo from '../../assets/images/image001.png';
import './logo.scss';
const Logo = () => (
  <Link to="/" className="logo_wrap">
    <div className="logo_wrap_logo">
      <img src={ logo } />
    </div>
  </Link>
);

export default Logo;
