import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

export const config = {
  apiKey: "AIzaSyBbhwEKai3IeNxenNZRTTI4qX6g5E1xOmI",
  authDomain: "pizza-online-wind.firebaseapp.com",
  databaseURL: "https://pizza-online-wind.firebaseio.com",
  projectId: "pizza-online-wind",
  storageBucket: "pizza-online-wind.appspot.com",
  messagingSenderId: "533926768658",
  appId: "1:533926768658:web:32a35c56f1a87b3d2168cb",
  measurementId: "G-VT5HSM3S2X"
};

firebase.initializeApp(config);
export const auth = firebase.auth();
export const firestore = firebase.firestore();
