import React from 'react';
import { Redirect } from 'react-router-dom';
import Login from '../views/login/login';
import Home from '../views/home/home';
import Menu from '../views/menu/menu';
import Carts from '../views/carts/carts';
import Profile from '../views/profile/profile';

export const publicRoutes = [
  {
    component: Login,
    exact: true,
    path: '/',
    title: 'Login',
  }
];

export const Private = [
  {
    component: Home,
    exact: true,
    path: '/',
    title: 'Login',
  },{
    component: Menu,
    exact: true,
    path: '/menus',
    title: 'Login',
  },{
    component: Carts,
    exact: true,
    path: '/cart',
    title: 'Cart',
  },{
    component: Profile,
    exact: true,
    path: '/profile',
    title: 'Profile',
  }
];