import React from 'react';
import { Redirect, Router, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import * as userProfileDispatcher from "../actions/userProfileDispatcher"
import history from './history';
import { publicRoutes, Private } from './routes';
import Layout from '../components/layout/index';
import Loader from '../components/loader';
import { currencyChanged } from '../utils';

class Routes extends React.PureComponent {
  state = {
    loading: true
  }

  async componentDidMount() {
    const localAuth = localStorage.getItem('isAuth')
    const usd_euro = localStorage.getItem('usd_euro')
    if(!usd_euro){
      const res = await currencyChanged()
      localStorage.setItem('usd_euro', res)
    }
    if (localAuth) {
      const user = localStorage.getItem('user')
      await this.props.actions.loginSuccess(JSON.parse(user))
    }
    this.setState({ loading: false });
  }

  componentDidUpdate(){
    const { globalMsg, actions } = this.props
    if(globalMsg){
      toast.success(globalMsg);
      actions.messageUpdate("")
    }
  }
  render() {
    const { user: { isAuth } } = this.props
    if (this.state.loading) {
      return (<Loader />)
    }
    const localAuth = localStorage.getItem('isAuth')
    let routes;
    routes = (isAuth || localAuth) ? Private : publicRoutes;

    return (
      <Router history={history}>
        <ToastContainer
          position="top-center"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
        <Switch>
          {routes.map((route, index) => {
            const { component: RouteComponent, exact, path } = route;

            return (
              <Route
                exact={exact}
                key={index}
                path={path}
                render={(props) => (
                  <Layout {...this.props}>
                    <RouteComponent {...props} />
                  </Layout>
                )} />
            );
          })

          }
          <Route path="" render={() => (<Redirect to="/" />)} />
        </Switch>
      </Router>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.authoringContentStore,
  globalMsg: state.authoringContentStore.globalMsg,
  isLoading: state.authoringContentStore.isLoading
})

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators({ ...userProfileDispatcher }, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(Routes);
