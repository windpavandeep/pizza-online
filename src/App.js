import React, { Component, Suspense } from 'react';
import logo from './logo.svg';
import './App.css';
import Routes from './routes';
import { auth } from "../src/config/index";
import store from './store';
import Loader from './components/loader';
import { Provider } from 'react-redux';

class App extends Component {
  state = {
    user: null
  };

  componentDidMount = () => {
    auth.onAuthStateChanged(userAuth => {
      this.setState({ user: userAuth });
    });
  };

  render() {
    return (
      <Suspense fallback={<Loader />}>
        <Provider store={store}>
            <Routes />
        </Provider>
      </Suspense>
    )
  }
}

export default App;