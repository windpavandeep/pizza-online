import React from "react";
import ListIcon from '@material-ui/icons/List';
import HomeIcon from '@material-ui/icons/Home';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';

export const nav = [
    {
        title:'Home',
        route:'/',
        icon:<HomeIcon />,
        bages: false
    },{
        title:'Menu',
        route:'/menus',
        icon:<ListIcon />,
        bages: false
    },{
        title:'My Cart',
        route:'/cart',
        icon:<ShoppingCartIcon />,
        bages: true
    },{
        title:'Profile',
        route:'/profile',
        icon:<AccountCircleIcon />,
        bages: false
    }
]