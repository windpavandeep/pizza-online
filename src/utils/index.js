import React from 'react';

export const updateObject = (initialObject, newParams) => {
  return {
    ...initialObject,
    ...newParams
  }
}


export const currencyChanged = async () => {
 return await fetch('https://free.currconv.com/api/v7/convert?q=USD_EUR&compact=ultra&apiKey=9f0ec556828b9036fede', {
    method: 'GET'
  }).then((res) => res.json())
    .then((data) => {
      return data.USD_EUR
    })
    .catch((err) => console.log(err))
}